## next is statically build in ngetty-helper.  Do not change !!!
NGETTY_DIR = /etc/ngetty

DESTDIR		= /
prefix		= ${DESTDIR}
sbin_prefix	= ${prefix}/sbin
etc_prefix	= ${prefix}/etc
ngetty_prefix	= ${etc_prefix}/ngetty
usrbin_prefix	= ${prefix}/usr/bin
mandir		= ${prefix}/usr/share/man
man8dir		= ${mandir}/man8

### next are aditinal options for make install
# P = -o root -g root

CC = gcc
CFLAGS = -Wall -Os
LDFLAGS = -s
FLAG_DEBUG = no

ALL_EXEC = ngetty ngetty.tiny ngetty.sortpfd \
 ngetty-helper ngetty-helper.tiny ngetty-argv \
 cleanutmp dumputmp nwho
ALL_MAN = ngetty.8.gz ngetty-helper.8.gz ngetty-argv.8.gz

ifeq ($(FLAG_DEBUG),no)
CCC_ = @echo '	CC $< ' ;
CCL_ = @echo '	CL $<	-> $@ ' ;
C = @
else
CCC_ =
CCL_ =
C =
endif

CC_C = $(CC) $(CFLAGS)
CC_L = $(CC) $(CFLAGS) $(LDFLAGS)

CCC = $(CCC_) $(CC_C)
CCL = $(CCL_) $(CC_L)
STR = $(C) strip -R .comment -R .note

ifeq (${prefix},/)
prefix = 
endif

MYARCH:=$(shell uname -m | sed -e 's/i[4-9]86/i386/' -e 's/armv[3-6]t\?e\?[lb]/arm/')

ALL = $(ALL_EXEC) $(ALL_MAN)
all: $(ALL)

%.o: %.c lib.h utmp_struct.h all_defs.h sysinfo_defs.h boottime_defs.h
	$(CCC) -c $<

STR_O	= $(patsubst %.c,%.o,$(wildcard str_*.c s*mem.c out_*.c))
FMT_O	= $(patsubst %.c,%.o,$(wildcard fmt_*.c))
SCAN_O	= $(patsubst %.c,%.o,$(wildcard x_atoi.c))
DJB_O	= $(STR_O) $(SCAN_O) $(FMT_O)
UTMP_O	= $(patsubst %.c,%.o,$(wildcard *_io.c *_do.c *_users.c *_devlog.c))
TIME_O	= $(patsubst %.c,%.o,$(wildcard *gmtime.c get_up*.c))
MISC_O	= $(patsubst %.c,%.o,$(wildcard first_*.c fork_*.c check_*))
ALL_O = $(UTMP_O) $(TIME_O) $(MISC_O)

opts_make: opts_make.c str_defs.h fmt_defs.h lib.a vhangup_defs.h lib.a
	$(CCL) -o $@ $< lib.a

all_defs.h: $(patsubst %.o,%.c,$(ALL_O)) tzmap.c ngetty-helper.c
	$(C) ./get_headers $@ -Gsys/stat.h -Gtime.h \
 -Lstr_defs.h -Lfmt_defs.h -Lscan_defs.h -Lutmp_struct.h $^ > $@
fmt_defs.h: fmt_*.c
	$(C) ./get_headers $@ $^ > $@
scan_defs.h: x_atoi.c const_io.c
	$(C) ./get_headers $@ $^ > $@
str_defs.h: str_*.c splitmem.c out_*.c const_io.c
	$(C) ./get_headers $@ $^ > $@
utmp_defs.h: utmp_do.c
	$(C) sed -n -e '/EXTRACT_START/,/EXTRACT_END/p' $< > $@
opts_defs.h: opts_make
	./opts_make | sed -n -e '/ O[odmt]/p' > opts__defs.h
	./opts_make > $@

sysinfo_defs.h: trysysinfo.c
	$(C) ( ( rm -f $@Z; $(CC_L) -o $@Z $< && ./$@Z ) > /dev/null 2>&1 \
	&& echo '#define HAVE_SYSTEM_SYSINFO' || true ) > $@
	$(C) rm -f $@Z; cat $@
boottime_defs.h: tryboottime.c
	$(C) ( ( rm -f $@Z; $(CC_L) -o $@Z $< && ./$@Z ) > /dev/null 2>&1 \
	&& echo '#define HAVE_SYSTEM_BOOTTIME' || true ) > $@
	$(C) rm -f $@Z; cat $@
vhangup_defs.h: tryvhangup.c
	$(C) ( ( rm -f $@Z; $(CC_L) -o $@Z $< ) > /dev/null 2>&1 && \
	echo '#define HAVE_SYSTEM_VHANGUP' || true ) > $@
	$(C) rm -f $@Z; cat $@

lib.a: $(patsubst fmt_time.o,,$(DJB_O)) $(ALL_O)
	ar cr lib.a $^
tzmap.a: tzmap.o fmt_time.o
	ar cr $@ $^
tzmap_mmap.a: tzmap_mmap.o
	ar cr $@ $^


ngetty_h = all_defs.h sortpfd.h sig_action.h
ngetty-helper_h = tzmap.a opts_defs.h lib.a

diet_flags = -isystem /opt/diet/include -D__dietlibc__ \
 -Os -mpreferred-stack-boundary=2 -falign-functions=1 -falign-jumps=1 \
 -falign-loops=1 -fomit-frame-pointer -DNGETTY_TINY -DNGETTY_$(MYARCH)

system-$(MYARCH).o: system-$(MYARCH).S
	$(CCC_) $(CC) $(diet_flags) -c $<
ngetty.$(MYARCH): ngetty.c  $(ngetty_h) system-$(MYARCH).o
	$(CCL_) $(CC) -nostdlib $(diet_flags) system-$(MYARCH).o \
 -s -Wl,-N -o $@ $<
	$(STR) $@

ngetty: ngetty.c $(ngetty_h)
	$(C) $(CC) --version || true; uname -a || true
	$(C) echo "ARCH = $(MYARCH)"; echo "CC = $(CC)"; \
 echo "CFLAGS = $(CFLAGS)"; echo "LDFLAGS = $(LDFLAGS)"; 
	$(CCL) -DNGETTY_SELFPIPE -o $@ $<
	$(STR) $@

ngetty.sortpfd: ngetty.c $(ngetty_h)
	$(CCL) -DNGETTY_SELFPIPE -DNGETTY_SORT -o $@ $<
	$(STR) $@
ngetty.tiny: ngetty.c $(ngetty_h)
	$(CCL) -DNGETTY_TINY -o $@ $<
	$(STR) $@
ngetty-helper: ngetty-helper.c $(ngetty-helper_h)
	$(CCL) -DNGETTY_HOME=\"$(NGETTY_DIR)\" -o $@ $< lib.a tzmap.a
	$(STR) $@
ngetty-helper.tiny: ngetty-helper.c $(ngetty-helper_h)
	$(CCL) -DNGETTY_HOME=\"$(NGETTY_DIR)\" -DDONT_NEED_ESC_TIME -DDONT_NEED_ESC_USERS -o $@ $< lib.a
	$(STR) $@

%: %.c
	$(CCL) -o $@ $^
	$(STR) $@

tzmap_mmap.c: tzmap.c
	$(C) ( echo '#define TZ_MMAP'; echo '#include "tzmap.c"' ) > $@

all_defs.h:	str_defs.h scan_defs.h fmt_defs.h utmp_defs.h
cleanutmp:	lib.a
ngetty-argv:	lib.a
dumputmp:	tzmap_mmap.a lib.a
nwho:		tzmap_mmap.a lib.a

str_%.o:	str_defs.h splitmem.c
fmt_%.o:	fmt_defs.h
fmt_time.o:	opts_defs.h
tzmap.o:	opts_defs.h

ngetty.8.gz: ngetty.8 ngetty-helper dumputmp ngetty-argv
	gzip -9 < $< > $@
	$(C) ls -la /var/run/utmp* /dev/tty1 /dev/vc/1 2>/dev/null || true
	./dumputmp < /var/run/utmp | head || true
	date '+%nCurrent time:  %Y-%m-%d %T	%Z %z'
	./ngetty-argv :-N:-a1:./ngetty-helper::TestTime || true
ngetty-%.8.gz:
	$(C) echo '.so ngetty.8' | gzip -9 > $@

install_other: $(ALL)
	install -m 755 $(P) -d $(usrbin_prefix)
	install -m 755 $(P) nwho dumputmp $(usrbin_prefix)
	install -m 2711 -o root -g utmp cleanutmp $(usrbin_prefix) || \
	  install -m 755 $(P) cleanutmp $(usrbin_prefix)
	install -m 755 $(P) ngetty.sortpfd ngetty.tiny ngetty-helper.tiny $(sbin_prefix)

install: $(ALL)
	install -m 755 $(P) -d $(sbin_prefix)
	install -m 755 $(P) -d $(etc_prefix)
	install -m 700 $(P) -d $(ngetty_prefix)
	install -m 755 $(P) -d $(man8dir)
	install -m 755 $(P) ngetty ngetty-helper ngetty-argv $(sbin_prefix)
	install -m 644 $(P) ngetty.8.gz ngetty-*.8.gz $(man8dir)
	install -m 600 $(P) sample.Conf $(ngetty_prefix)
	install -m 755 $(P) contrib/setup $(ngetty_prefix)
	install -m 644 $(P) Version $(ngetty_prefix)
	test -f $(ngetty_prefix)/Conf || \
	  install -m 600 $(P) Conf $(ngetty_prefix)
	@echo 
	@echo '   If you want to install some additional programs do:'
	@echo '   make install_other'

clean:
	rm -f $(ALL) *.o *.a *.s *_defs.h *.hZ a.out tzmap_mmap.c \
 *.c.orig test-helper opts_make ngetty.$(MYARCH) sstrip.* elf_print*
	rm -rf PACKAGE 
distclean:
	make clean
	rm -rf d dietlibc

withdiet:
	@echo '	Please, start:	make CC="diet -Os gcc"'
dietlibc/bin-$(MYARCH)/diet:
	cvs -d :pserver:cvs@cvs.fefe.de:/cvs -z9 co dietlibc
	cd dietlibc && make
dietbuild: dietlibc/bin-$(MYARCH)/diet
	DIETHOME=$(CURDIR)/dietlibc make CC="./$< -Os gcc -nostdinc"


AUTHORS_SITE = http://riemann.fmi.uni-sofia.bg
sstrip.bin: sstrip.c
	$(CCL) -DNV_PRINT_ELF_HEADER -o elf_print $^
	./elf_print < elf_print > elf_defs.h
	$(CCL) -o $@ $^
	rm -f elf_print*
sstrip.c:
	rm -f $@
	wget -O $@ $(AUTHORS_SITE)/programs/$@
sstrip: $(ALL_EXEC) sstrip.bin
	./sstrip.bin $(ALL_EXEC) ngetty.$(MYARCH) || true

TAR=tar
TAR_OPT=
MY_NAME=$(notdir $(CURDIR))
GZIP_PR = 7za a -tgzip -si -so -bd Z

tar: 
	cd .. && $(TAR) -cv $(TAR_OPT) \
 --owner=root --group=root --exclude $(MY_NAME)/dietlibc $(MY_NAME) | \
$(GZIP_PR) > $(MY_NAME).tar.gz
packit:
	( echo $(MY_NAME); date -u ) > Version
	make clean tar TAR=tar.f TAR_OPT=--sort
package:
	make all install install_other DESTDIR=PACKAGE P=
	install ngetty.$(MYARCH) PACKAGE/sbin || true
	install contrib/*install* PACKAGE
	cd PACKAGE && tar -cvzf /tmp/$(MY_NAME)-$(MYARCH).tar.gz \
 --owner=root --group=root *
rpm: $(MY_NAME).spec
	rpmbuild -ba --clean $<
