#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include "lib.h"

/* type:  F_RDLCK or F_WRLCK */
struct utmp_type *utmp_io(int fd, struct utmp_type *ut, int type) /*EXTRACT_INCL*/ {
  struct flock fl;
  int len;
  
  fl.l_whence	= SEEK_CUR; 
  fl.l_start	= 0;
  fl.l_len	= UTMP_SIZE;
  fl.l_pid	= 0;
  fl.l_type	= type;
  
  if (fcntl(fd, F_SETLKW, &fl)) return 0;
  len = const_io((type==F_WRLCK) ? (int(*)())write : (int(*)())read,  
		 fd, ut, UTMP_SIZE);

  fl.l_start	= -UTMP_SIZE;
  fl.l_type	= F_UNLCK;

  fcntl(fd, F_SETLK, &fl);

  return  (len) ? 0 : ut;
}
