#include "str_defs.h"

unsigned int str_copynz(char *dest, const char *src, unsigned int n) /*EXTRACT_INCL*/ {
  unsigned int k=str_add(dest, src, n);
  while (k<n) dest[--n] = 0;
  return k;
}
