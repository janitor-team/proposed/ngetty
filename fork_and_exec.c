#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
extern char **environ;

void fork_and_exec(char *line) /*EXTRACT_INCL*/{
  if (line) {
    char *qq[4] = { "/bin/sh", "-c", line, 0 };
    pid_t pid=fork();
    if (pid==-1) return;
    if (pid==0) { execve(*qq,qq,environ); _exit(127); }
    waitpid(pid,0,0);
  }
}
