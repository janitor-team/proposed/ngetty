#define fmt_number_macro(f,type,base) \
unsigned int f(char *s, type u) {\
  type tmp=u;\
  unsigned int len=0;\
  do { tmp /=base; ++len; } while(tmp);\
  if (s) {\
    s +=len;\
    do {\
      unsigned char c = u%base;\
      if (base <= 10) *--s = c+'0';\
      else *--s = (c<10) ? c+'0' : c-10+'a';\
      u /=base;\
    } while (u);\
  }\
  return len;\
}
