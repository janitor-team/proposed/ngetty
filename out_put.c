#include <unistd.h>
#include "str_defs.h"

static char buf_space[256];
static int  buf_pos;

void out_flush() /*EXTRACT_INCL*/ { 
  const_io((int(*)())write, 1, buf_space, buf_pos);
  buf_pos=0;
}

void out_char(char ch) /*EXTRACT_INCL*/ {
  if (buf_pos == sizeof(buf_space)) out_flush();
  buf_space[buf_pos] = ch;
  buf_pos++;
}

void out_puts(char *s) /*EXTRACT_INCL*/ { 
  while (*s) {
    out_char(*s);
    ++s;
  } 
}
