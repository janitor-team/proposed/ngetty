#include <sys/types.h>
#include <sys/sysctl.h>
#include <sys/time.h>
#include <time.h>

int main() {
#if defined(CTL_KERN) && defined(KERN_BOOTTIME)
  /* FreeBSD specific: fetch sysctl "kern.boottime". */
  struct timeval res = { 0, 0 };
  int req[2] = { CTL_KERN, KERN_BOOTTIME };
  size_t res_len = sizeof res;
  if (sysctl(req, 2, &res, &res_len, 0, 0) >= 0 && res.tv_sec > 0)
    return 0;
#else
  return -1;
#endif
}
