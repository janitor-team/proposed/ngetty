#include <unistd.h>
#include <errno.h>

int const_io(int (*op)(), int fd, void *buf, int len) /*EXTRACT_INCL*/{
  char *x = buf;
  while (len > 0) {
    int ret = op(fd, x, len);
    if (ret <= 0) {
      if (ret && errno == EINTR) continue;
      break;
    }
    x += ret;
    len -= ret;
  }
  return len;
}
