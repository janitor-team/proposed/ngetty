#include <time.h>

struct tm *nv_gmtime(const time_t *t) /*EXTRACT_INCL*/{
  static struct tm tm;
  unsigned long day, mon, year, yday=0, tod;

  tod = (unsigned long)(*t) % 86400;
  day = (unsigned long)(*t) / 86400;
  tm.tm_wday = ((day+4) % 7);
  tm.tm_sec  = tod%60; tod /= 60;
  tm.tm_min  = tod%60; 
  tm.tm_hour = tod/60;

  year = 4*day + 2;
  year /= 1461;

  day += 671;
  day %= 1461;           /* day 0 is march 1, 1972 */
  if (day < 306) yday = 1;
  if (day == 1460) { day = 365; yday = 59; }
  else { day %= 365;   yday += (day + 59) % 365; }

  day *= 10;
  mon = (day + 5) / 306;
  day = day + 5 - 306 * mon;
  day /= 10;
  if (mon >= 10) mon -= 10;
  else mon += 2;      /* day 0,1,30, mon 0..11,  year 1970=0,1,2 */

  tm.tm_year = year+70;
  tm.tm_mon  = mon;
  tm.tm_mday = day+1;
  tm.tm_yday = yday;
  return &tm;
}

