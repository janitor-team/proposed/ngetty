#ifndef LIB_H
#define LIB_H

/* Pathname of the login(1) program. */
#define LOGIN "/bin/login"

/* Start ngetty with ohter appl:  /sbin/ngetty -H/path/to/helper 1 2 3 */
// #define NEED_HELPER_OPTION

/* Define one of the next to use libc utmp() or utmpx() functions */
// #define HAVE_C_UTMP
// #define HAVE_C_UTMPX

/* Poll only nonnegative fd */
// #define NGETTY_SORT

/* ----- stop changes here ----- */

/* Pathname of the ngetty-helper program. */
#define NGETTY_HELPER "/sbin/ngetty-helper"

extern char **environ;

#define GLOBAL_fstat_READ(fd,st,s, len,max_len, plus) \
  fstat(fd,&st) || (unsigned int)(len=st.st_size) > max_len || \
  (s=alloca(len+plus))==0 || const_io((int(*)())read,fd,s,len)

#define GLOBAL_split_plus(aa,s,ch, len,plus) do { /* aa must be 0 !!! */ \
  len=splitmem(aa,s,ch); \
  if (aa) break; \
  aa=alloca((len+plus) * sizeof(char*)); } while(1)

#define GLOBAL_split(aa,s,ch, len) GLOBAL_split_plus(aa,s,ch, len,1)

#include "all_defs.h"
#endif
