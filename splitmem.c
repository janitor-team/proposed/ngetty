unsigned int splitmem(char **v, char *s, char c) /*EXTRACT_INCL*/ {
  char **w=v;
  unsigned int n=1;

  if (w) *w++=s;
  while (*s) {
    ++s;
    if (s[-1] != c) continue;
    ++n;
    if (w==0) continue;
    *w++=s;
    s[-1]=0;
  }
  if (w) *w=0;
  return n;
}
